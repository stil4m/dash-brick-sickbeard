import cherrypy
from app.controllers import BaseController
from jinja2 import Environment, FileSystemLoader

class SickBeardController(BaseController):

    def __init__(self, show_store, prefix, settings, update_callback):
        self.plugin_name = prefix.split('/')[-1]
        self.update_callback = update_callback
        self.sick_beard_settings = settings
        self.env = Environment(loader=FileSystemLoader(prefix.lstrip('/') + '/templates'))
        BaseController.__init__(self)
        self.show_store = show_store

    @cherrypy.expose
    def index(self):
        template = self.env.get_template('index.html')
        shows = self.show_store.all_shows()
        error = self.show_store.get_error()
        return template.render(shows=shows, error=error, name=self.plugin_name)

    @cherrypy.expose
    def settings(self):
        template = self.env.get_template('settings.html')
        return template.render(
            name=self.plugin_name,
            host=self.sick_beard_settings.host,
            apikey=self.sick_beard_settings.key
        )

    @cherrypy.expose
    def save_credentials(self, host, apikey):
        self.sick_beard_settings.update_host_and_key(host, apikey)
        self.update_callback()
        return self.post_success_with_data('Data')

