__author__ = 'mstijlaart'

import json
import urllib2

from .downloadedhistoryentry import DownloadedHistoryEntry

class SickBeardApi:
    def __init__(self, settings):
        self.settings= settings

    def get_history(self, limit = 5):
        try:
            url = self.settings.host + '/api/' + self.settings.key + "?cmd=history&type=downloaded&limit=" + str(limit)
            req = urllib2.Request(url)
            response = urllib2.urlopen(req)
            the_page = response.read()
            json_object =  json.loads(the_page)

            if json_object['result'] == "success":
                entries = []
                for history_entry in json_object['data']:
                    entries.append(DownloadedHistoryEntry(history_entry))
                return entries
            else:
                return json_object['message']
        except:
            return "Something went wrong. Please check the Sick-Beard settings."