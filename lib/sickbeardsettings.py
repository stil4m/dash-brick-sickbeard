__author__ = 'mstijlaart'

import ConfigParser

class SickbeardSettings:

    def __init__(self, config_file_path):
        self.config_file = config_file_path
        self.configParser = ConfigParser.RawConfigParser()
        self.configParser.read(self.config_file)
        self.key = self.try_to_get_string('sickbeard', 'apikey')
        self.host = self.try_to_get_string('sickbeard', 'host')
        self.handle_host()

    def try_to_get_string(self, section, option):
        try:
            return self.configParser.get(section, option)
        except (ConfigParser.NoOptionError, ConfigParser.NoSectionError):
            pass
        return ''

    def update_host_and_key(self, host, key):
        self.try_to_set_string(host, 'sickbeard', 'host')
        self.try_to_set_string(key, 'sickbeard', 'apikey')
        with open(self.config_file, 'wb') as configfile:
            self.configParser.write(configfile)
        self.key = self.try_to_get_string('sickbeard', 'apikey')
        self.host = self.try_to_get_string('sickbeard', 'host')

    def try_to_set_string(self, value, section, option):
        try:
            self.configParser.set(section, option, value)
        except (ConfigParser.NoOptionError, ConfigParser.NoSectionError):
            pass

    def handle_host(self):
        self.host = self.host.strip().rstrip('/')
        if self.host.find("http://") is -1:
            self.host = "http://" + self.host

    @property
    def get_host(self):
        return self.host

    @property
    def get_key(self):
        return self.key