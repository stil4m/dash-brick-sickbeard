__author__ = 'mstijlaart'

class ShowStore:

    def __init__(self):
        self.shows = []
        self.error = None

    def append(self, o):
        self.shows.append(o)
        self.error = None

    def extend(self, items):
        self.shows.extend(items)
        self.error = None

    def set_error(self, error):
        self.error = error
        del self.shows[:]

    def all_shows(self):
        return self.shows

    def get_error(self):
        return self.error


