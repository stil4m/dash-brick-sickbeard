__author__ = 'mstijlaart'

from sickbeardapi import  SickBeardApi

class Updater:

    def __init__(self, settings, show_store):
        self.api = SickBeardApi(settings)
        self.show_store = show_store

    def execute(self):
        result = self.api.get_history()
        if isinstance(result, basestring):
            self.show_store.set_error(result)
        else:
            self.show_store.extend(result)