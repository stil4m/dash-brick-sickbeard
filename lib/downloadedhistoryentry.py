__author__ = 'mstijlaart'

class DownloadedHistoryEntry:

    def __init__(self, json_dict):
        self.show_name = json_dict['show_name']
        self.season = json_dict['season']
        self.episode = json_dict['episode']

    @property
    def get_show_name(self):
        return self.show_name

    @property
    def get_season(self):
        return self.season

    @property
    def get_episode(self):
        return self.episode

    def full_name(self):
        return '%(show_name)s S%(season)02dE%(episode)02d' % {
            'show_name': self.show_name,
            'season': self.season,
            'episode': self.episode
        }