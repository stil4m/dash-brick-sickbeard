function sickbeard_class(name) {

    var thisObject = this;
    this.name = name;
    var elementID = this.name;
    var path = '/plugins/' + elementID;
    var settingsElementId = this.name + 'Settings'


    this.render = function() {
        dash.render(path + '/', elementID);
    };

    this.settings = function() {
        dash.modalSettings(
            path + '/settings',
            elementID,
            settingsElementId,
            this.render
        );
    };

    this.exitSettings = function () {
      dash.hideModalSettings(settingsElementId);
    };

    this.saveCredentials = function (element) {
        var buttonElement = $(element);
        buttonElement.button('loading');
        $.get(
            path + '/saveCredentials',
            {
                'host': $(element).parent().find('#host').val(),
                'apikey': $(element).parent().find('#apikey').val()
            },
            function(responseJson) {
                dash.reloadSettings(path + '/settings', settingsElementId)
            },
            'json'
        );
    };

};