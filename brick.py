__author__ = 'mstijlaart'

from .lib.sickbeardsettings import SickbeardSettings
from .lib.updater import Updater
from .lib.showstore import ShowStore
from .sickbeardcontroller import SickBeardController

class Brick():

    def __init__(self, mapper, prefix, path):
        self.mapper = mapper
        self.prefix = prefix
        self.path = path
        self.name = self.prefix.split('/')[-1]
        self.settings = SickbeardSettings(self.path + '/config.ini')
        self.show_store = ShowStore()
        self.updater = Updater(self.settings, self.show_store)
        self.setup_mapper()

    def setup_mapper(self):
        if self.prefix and self.mapper:
            sick_beard_controller = SickBeardController(show_store=self.show_store, prefix=self.prefix, settings=self.settings, update_callback=self.update)
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/'), controller=sick_beard_controller, action = 'index')
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/settings'), controller=sick_beard_controller, action = 'settings')
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/saveCredentials'), controller=sick_beard_controller, action = 'save_credentials')

    def update(self):
        self.updater.execute()

    def get_javascript(self):
        js = "<script type=\"text/javascript\" src=\""+ self.prefix + "/brick.js\"></script>\n"
        js += "<script> var " + self.name + " = new sickbeard_class('"+ self.name +"')</script>\n"
        js += "<script>" + self.name + ".render()</script>"
        return js

    def timeout(self):
        return 3600

    def get_real_name(self):
        return 'dash-brick-sickbeard'